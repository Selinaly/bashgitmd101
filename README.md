# Bash and Markdown 101 :taco:

## Intro 

This repository is our documentation on bash and markdown. We will put here the main commands of basg and how to document it in markdown.

We will learn (this is an unordered list):
- bash
- markdown

First we'll learn (this is a list):
1. bash 
2. markdown

## Bash

Bash is a language that speaks with the Kernal. Used throughout linux machines (90%)


### Main Commands
Write here the **`main commands`**

#### Where am I?
    $ pwd
    > /user/path/location

#### Where can I go?
Short list:
​

    $ ls
    > dir_a  dir_b  file

#### Short list all (shows hidden directories):
    
   
     $ ls -a
   
    > dir_a dir_b dir_c .hid_a .hid_b
​
Long list:
    
    $ ll
    
    $ ll -a
#### Go somewhere

    $ cd <path/directory>
    > ~/<path/directory>/
#### Come back
    $ cd ..
    > ~

#### Play creationist
    $ touch <path> <path>

#### Create a folder/directory 
    $ mkdir <new dir>
    $ ls
    > <new dir>
#### Remove a file
    $ rm <path/target>
#### Remove a directory
    $ rmdir <path/dir/>
    $ rm -rf <path/dir/>

#### Print file 
    $ cat example.file

#### Printing to console
    $ echo 'hello'
    > hello

#### Truncate output to file
    $ echo 'hello' > example.file
    $ cat example.file
    > hello

#### Append output to file
    $ echo 'my name is' >> example.file
    $ cat example.file
    > hello
    > my name is

#### To kill the process on the terminal 
    
    - Ctrl + C