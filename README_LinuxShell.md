# Linux Shell

## Intro

This is a document denoting basic linux commands and their functions. This also includes information on commands used within a server `ssh`, shell directives (including wildcards), and the order of execution operation when a command is run.


## Shell Directives:

Things that shell has to do before executing your command

Order:
1. Press enter
2. Seperates into an array based on the spacing on the command line
3. Sifts through array and identifies anything the shell nees to do
4. It will then process each directive
   ```
   $ cd /
   ```
5. As it processes each directive it rewrites the comamnd line
6. Then it searches through $ PATH looking fro where he actual command is 
7. It will run the command based on it's absolute path
8. replaces the code for the shell with the code of the command 
   - overwrites the bash function in the memory with the command
   


## Note On Formatting
​
Shell commands (e.g. `cmd`) are denoted in `this` format.
​
### Example Format:
​
`cmd1` - Description of Command  1
​
---
​
## Shell commands
​
- `ssh selina@linux.academy.grads.al-labs.co.uk` - To login through SSH
- `who` - To see who has logged on:
- `ls -la`: List information with "long all" option for a lot more information
​
    ### **Important To Remember Operation Execution Order**
    - `command` + `option(s)` + `argument` 
    - For example `ls -i /etc/` is broken up as `ls` + `-i` + `/etc/`
        - This translates to `list the contents` + `including the node number` + `in the directory /etc/`
    - This order can be manipulated
​
---
​
## The Timeline of Operations When A Command Is RUn
​
1. A `command` is written in the Shell and Executed.
   - asds
​
## Wildcards (*, p [ ], ?)

​
- `ls -d /etc/p*` show me all files beginning with p 
  - the `-d` stops the directory from showing the contents and show it as a folder
  - `-i` is the i-node number, a unique number for every file/folder
​
- `ls -d /etc/*d` all ENDING with d
- `ls -d /etc/*z*` all including with z
- `ls -d /etc/[aeiou]*` all starting with a,e,i,o,u
- `ls -d /etc/[aeiou]*` all starting with a,e,i,o,u
​
​
- `man ls` is a manual about ls?
- `man man` is a manual on manual
- `ls -d /etc/[A-Z]*` searches for files starting with upper case
  - need to `export LANG=C` to convert to ascii for above cmd to be possible
- `ls -d /etc/a[A-Z0-9]*` forgot to listen to this one
- `ls -d /etc/????` to searach for file names with 5 characters
- if `ls` for some reason died
  - you can use `echo /etc/p*` to print out the files
  - echo prints the expanded of `/etc/p*`
​
- You can use `echo` before any command to see what the command being run will be because the echo would print it out 
​
​
- copy is `cp a acopy` copies a as acopy
- but `cp a b x` then files a and b gets copied into x
- `|` pipe takes the standard output and switches as an input for another comand
​
​
​
​
## Timeline of running a command
​
`You write a command and press enter`
​
It parses the command line
​
- Shell Directives:
  - Things that shell has to do before executing your command
    - Wildcards (*, p [ ], ?)
      - These are filenames generations
    - Quotes ' " \
    - Subshell `cmd` $(cmd)
      - Runs a "subcommand" for the main command
    - Redirection > >> 2> 2>> <
      - Get's information as a program
      - `>` redirects the standard ouput from the preceding command and sends it to a file you give it
        - also overwrites
      - `>>` appends the redirected input line/creates
      - `2>` is standard error as output
      - `<` gets an input from a location, instead of standard input
    - Pipelines `cmd | cmd`
      - Command on the right becomes input for command on the right
    - Variables $PATH
  - The shell expans these directives and sets it up as variables for the cmd to execute on
  
- It then rewrites the command line to substitute the above
- Then searches for where the commands live 