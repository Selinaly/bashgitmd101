# Linux 101

## Steve's Questions

### What is the only character that cannot be used in a Linux/Unix file name?

    /

### Two ways to create a file name containing a space:

    $ touch file\ {1..3}
    > file 1 file 2 file 3

    $ touch "hello world "{1..3}
    > hello world 1 hello world 2 hello world 3

### Create file called -r and how to remove it:

- To create the file

      $ touch ./-r
    

- To remove the file

      $ rm ./-r

### Replace share with wildcard/ combination

    $ cd /usr/share/locale

    $ cd /usr/??a??/locale

## Commonly Used Commands

### Finding and getting to files

#### **file** 

Identify the type of file

    $ file examplefile.exe
    > examplefile.exe: ASCII text

#### **which**

Tells me where a command is coming from using directory locations defined by my environment. 

    $ which 

#### **find** 

Search for files or directories in the file system. 

    $ find 

#### **df**

Tell me how much of the file system(s) is in use or how the file system structure is made up.

#### **du**

Tell me which directories are using how much space.

## Exercise Answers
1. How to check you're logged into vagrant without logging out?
```  
    $ whoami
    $ id
```
2. Check you're in the home directory 

```   
- $ pwd 
  > /users/selinaly/home
- $ cd 
```

3. List all the files in your current directory 

``` 
   $ ls -a
```

4. Create a file called hello_world and .goodbye_world and check they are there

```
    $ touch hello_world
    $ echo "Boo" >hello_world
    $ cat >hello_world
    > Boo
    $ touch ".goodbye_world"
    $ ls -a
```

5. Create multiple directory structure in one command and check
   
```
$ mkdir -p {book,book/fiction,book/fiction/dystopian,book/non-fiction}
$ ls -R
```
6. Copy the /etc/passwd file into movies/comedy/films

```
$ cp /etc/passwd movies/comedy/films
```
7. Find out where the passwd command is and copy that file into movies/comedy/standup

```
$ which passwd
$ cp /usr/bin/asswd movies/comedy/standup
or
$ cp $(which passwd) movies/comedy/standup
```
8. Change directory to music and whilst in this directory rename your .goodbye_world so that it is in this directory instead, and no longer in your home directory.

```
$ cd music
$ mv ../.goodbye_world .
```
