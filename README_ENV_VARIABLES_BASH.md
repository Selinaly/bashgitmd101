# Enviroment Variable in BASH

In this class we looked at Variables in bash. The class 

## First Exercise 

This class included:
 - Setting a variable using command line `NAME=SELINA`
 - Calling a varibale `echo $NAME`
 - We logged out of our session in bash and logged back in
 - Variable was no longer ser


## Setting a varibale in `.bash_profile`

- Export creates a backup or to share the set with another user

```BASH
export PATH
echo Hello $LOGNAME
cal
export EDITOR=nano
export SECRET_KEY="secret123"
export Dumpling="Prawn"
export ENV=~/.bashrc
export DRINK=WATER
```

## Making a file that called a variable (Child process)

In a file called `files`
```BASH
#!/bin/bash


echo $NAME
echo $BOOKS
echo $DRINK
echo `Hello files`
```
## Morphing the variable using export inside the file, and calling another file

```BASH
#!/bin/bash


echo $NAME
echo $BOOKS
echo $DRINK
echo `Hello files`

export DRINK=GIN

~/hello
```
To see output: 

```BASH
$ ./files

> WATER
Hello files
Reading from hello
GIN
```
- Parent file told script 
```
> DRINK=WATER
```
- Editing Child file 
``` 
> DRINK=GIN
```
- Output shows
```
> GIN
```

## Adding more varibales to `.bash_profile`

```BASH
export PATH
echo Hello $LOGNAME
cal
export EDITOR=nano
export SECRET_KEY="secret123"
export Dumpling="Prawn"
export ENV=~/.bashrc
export DRINK=WATER
```
## Change access of PS

r = read
w = write 
x = execute 
u = user 

```BASH
> -rw'-'rw-r-- 1 selina selina 95 Apr 12 09:17 files
$ chmod u+x files
> -rw'x'rw-r-- 1 selina selina 95 Apr 12 09:17 files
```
